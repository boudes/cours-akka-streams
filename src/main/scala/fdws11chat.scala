package fr.mipn.fdws11

import scala.io.StdIn
import scala.concurrent.duration._
import scala.language.postfixOps
import org.slf4j.LoggerFactory

// import akka.actor.ActorSystem

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors

import akka.stream.scaladsl.MergeHub
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Keep
import akka.stream.scaladsl.Source
import akka.stream.scaladsl.BroadcastHub
import akka.stream.scaladsl.Flow

import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.model.ws.Message
import akka.http.scaladsl.model.ws.TextMessage
import akka.stream.OverflowStrategy
import akka.stream.OverflowStrategies
import akka.http.scaladsl.model.ws.BinaryMessage
import akka.util.ByteString
import akka.stream.scaladsl.Broadcast
import akka.stream.scaladsl.GraphDSL
import akka.stream.scaladsl.Merge
import akka.stream.FlowShape
import scala.util.Success
import akka.Done
import scala.util.Failure
import akka.http.scaladsl.model.ws.TextMessage.Strict

object Fdws11 extends App {
    implicit val system = ActorSystem(Behaviors.empty, "system")
    import system.executionContext
    val log = LoggerFactory.getLogger("monLogger")

    def createChatRoom(name: String) = {
        val mergehub = MergeHub.source[Message]
        val broadcasthub = BroadcastHub.sink[Message]
        mergehub.log(name).toMat(broadcasthub)(Keep.both).run()
    }

    val (main_talk, main_listen) = createChatRoom("General")

    case class ChatCommand(cmd: String)

    def wsFlow(starting_name: String, listen: Source[Message, _], talk: Sink[Message,_]): Flow[Message, Message, _ ] = {
      /* rend le service de chat au client
       * si le client dit /shrug ça dessine dans son message un ¯\_(ツ)_/¯
       * si le client dit /nick <nouveau_pseudo> ça change son nom
       * si le client envoie un autre /… son message est ignoré
       * si le client envoie un autre message, il est diffusé via le salon à tous les autres
       * clients.
       * De plus lorsque le client rejoint le salon et lorsqu'il le quitte, il est annoncé.
       * Enfin on utilise les messages binaires pour garder vivante la websocket. 
       * Un message binaire vide toutes les 30 secondes conserve active la connexion.
       */
 
      var name = starting_name /* le pseudo initial du client */
      val user = createChatRoom(name)
      /* Keep All WS connections alive by ticking empty binary Messages every 30 seconds */
      Source.tick(30 seconds, 30 seconds, BinaryMessage(ByteString.empty)).runWith(user._1)

      /* Annonçons notre arrivée dans le chat */
        val annonce = Source.single(TextMessage.Strict(s"$name est dans le salon"))
        val pickAndDecorateRegularText = Flow[Message].mapConcat({
            case Strict(s"/${command}") => 
                Nil
            case msg: TextMessage =>
                TextMessage(Source.single(name + " : ") ++ msg.textStream) :: Nil
            case msg: BinaryMessage => 
                msg.dataStream.runWith(Sink.ignore)
                Nil
        })
        val pickCommand = Flow[Message].collect[ChatCommand]({
            case Strict(s"/${command}") => ChatCommand(command)
        }).log("cmd")
        val executeCommand = Flow[ChatCommand].collect({
            case ChatCommand(s"shrug $texte") => 
                TextMessage.Strict(s"$name : $texte ¯\\_(ツ)_/¯")
            case ChatCommand(s"nick $newname") =>
                val msg = TextMessage.Strict(s"$name a changé de nom en $newname") 
                name = newname 
                msg
            case ChatCommand(s"join $chatroom") =>
                // TODO
                TextMessage.Strict(s"TODO rejoindre $chatroom") 
        })

        val chatroom = Flow.fromSinkAndSourceCoupled(talk, listen)

        val depart = Sink.onComplete({
          case Success(Done) => Source.single(TextMessage.Strict(s"$name a quitté le salon")).runWith(talk) 
          case Failure(e) => Source.single(TextMessage.Strict(s"""|$name ne reçoit plus les messages
                                                                  |(erreur : $e)""".stripMargin)).runWith(talk)
        })

        val buffer = Flow[Message].buffer(20, OverflowStrategy.dropHead)
        // TODO

         /* le flow de service de la la websocket */
        Flow.fromGraph(
            GraphDSL.create() {
                /* 0 en paramètre un builder pour construire des formes (shapes) */
                implicit b =>
                import GraphDSL.Implicits._
                /* 1 charger des formes (shapes) */
                val A = b.add(annonce)
                val D = b.add(depart)
                val S = b.add(pickAndDecorateRegularText)
                val C = b.add(pickCommand)
                val E = b.add(executeCommand)
                val chat = b.add(chatroom) 
                val T = b.add(buffer)
                val M = b.add(Merge[Message](3))
                val B = b.add(Broadcast[Message](3))

                /* 2 connecter par des arrêtes les formes */
                          A ~> M
                B  ~>  S   ~>  M ~> T ~> chat
                B ~> C ~> E ~> M    
                B ~> D
                // TODO
                // 3 retourner graphe Flow 
                FlowShape(B.in, chat.out)
            }
        )
        
    }

    val route: Route =
      pathEndOrSingleSlash {
        /* le client a envoyé 'GET /' on réponds avec le contenu d'un fichier */ 
        getFromResource("web/chat_with_name.html")
      } ~
        /* le client a demandé '/<truc>' ou <truc> ne contient pas de slash
         * on répond en essayant de trouver un fichier truc dans le répertoire
         */
        getFromResourceDirectory("web") // utile pour les images, les css etc.

    val apiRoute: Route =
      path( "api" / "chat") {
        get {
          parameters("name") { name =>
            /* le client a demandé GET /api/chat?name="bob" on lui répond avec un Flow */
            handleWebSocketMessages(wsFlow(name, main_listen, main_talk))
          }
        }
      }

    val servicePort = 9393
    val serviceInterface = "localhost"
    val bindingFuture = Http().newServerAt(serviceInterface, servicePort).bind(route ~ apiRoute)
    println(s"Server online at http://$serviceInterface:$servicePort/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done

}
