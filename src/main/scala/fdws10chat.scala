package fr.mipn.fdws10

import scala.io.StdIn
import scala.concurrent.duration._
import scala.language.postfixOps
import org.slf4j.LoggerFactory

// import akka.actor.ActorSystem

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors

import akka.stream.scaladsl.MergeHub
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Keep
import akka.stream.scaladsl.Source
import akka.stream.scaladsl.BroadcastHub
import akka.stream.scaladsl.Flow

import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.model.ws.Message
import akka.http.scaladsl.model.ws.TextMessage
import akka.stream.OverflowStrategy
import akka.stream.OverflowStrategies

object Fdws10 extends App {
    implicit val system = ActorSystem(Behaviors.empty, "system")
    import system.executionContext
    val log = LoggerFactory.getLogger("monLogger")

   // construire le Flow nécessaire, pour ça on utilise une matérialisation des deux hubs
    val mergehub1 = MergeHub.source[Message]
    val broadcasthub1 = BroadcastHub.sink[Message]
    val (matMerge1, matBcast1) = mergehub1.log("central").toMat(broadcasthub1)(Keep.both).run()

    def wsFlow1(name: String, inputs: Source[Message, _], outputs: Sink[Message,_]): Flow[Message, Message, _ ] = {
        Source.single(TextMessage.Strict(s"$name est dans le salon")).runWith(outputs)
        Flow.fromSinkAndSource(outputs, inputs)
    }

    val route: Route =
      pathEndOrSingleSlash {
        getFromResource("web/chat_with_name.html")
      } ~
        getFromResourceDirectory("web") // utile pour les images, les css etc.

    val apiRoute: Route =
      path( "api" / "chat") {
        get {
          parameters("name") { name =>
            handleWebSocketMessages(wsFlow1(name,matBcast1, matMerge1))
          }
        }
      }

    val servicePort = 9393
    val serviceInterface = "localhost"
    val bindingFuture = Http().newServerAt(serviceInterface, servicePort).bind(route ~ apiRoute)
    println(s"Server online at http://$serviceInterface:$servicePort/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done

}
