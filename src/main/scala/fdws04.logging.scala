package fr.mipn.fdws04

import akka.stream.scaladsl.Source
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.Sink
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors

import scala.concurrent.duration._
import scala.language.postfixOps
import akka.stream.scaladsl.RunnableGraph
import akka.stream.scaladsl.GraphDSL
import akka.NotUsed
import akka.stream.scaladsl.Broadcast
import akka.stream.ClosedShape
import org.slf4j.LoggerFactory

/* Documentation : https://doc.akka.io/docs/akka/current/typed/logging.html */

object Fdws04 extends App {
    implicit val system = ActorSystem(Behaviors.empty,"system") 
    import system.executionContext
    val log = LoggerFactory.getLogger("com.myservice.BackendTask")


    Source(1 to 12).log("source").async.via(Flow[Int].map("~~" + _ + "~~>").throttle(3,1 second)).log("map").async.runWith(Sink.ignore)
    

    Thread.sleep(4000)
    
    log.info("Graphe")

    val source0 = Source(1 to 10)
    val source1 = Source(15 to 20)
    val sink0 = Sink.ignore
    val sink1 = Sink.ignore
    val sink2 = Sink.ignore

    
    val graph = RunnableGraph.fromGraph(
        GraphDSL.create() {
            /* 0 en paramètre un builder pour construire des formes(shapes) */
            implicit builder: GraphDSL.Builder[NotUsed] =>
            import GraphDSL.Implicits._
            /* 1 charger des formes (shapes) */
            val broadcast = builder.add(Broadcast[Int](2))
   
            /* 2 connecter par des arrêtes les formes */
            /*
                                   |--- sink0
            source0 ---> broadcast-|
                                   |--- sink1

            source1 ---> sink2

            */
            source0.log("src0") ~> broadcast; broadcast.out(0).log("bc0") ~> sink0
                                              broadcast.out(1).log("bc1") ~> sink1
            
            source1.log("src1") ~> sink2           

            // 3 retourner graphe clos 
            ClosedShape
        }
    )
    graph.run()

    
    Thread.sleep(10000)
    log.info("Sayonara")
    system.terminate()
}