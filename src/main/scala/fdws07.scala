package fr.mipn.fdws07

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import org.slf4j.LoggerFactory
import akka.stream.scaladsl.MergeHub
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Keep
import scala.concurrent.duration._
import scala.language.postfixOps
import akka.stream.scaladsl.Source
import akka.stream.scaladsl.BroadcastHub
import akka.stream.scaladsl.Flow


object Fdws07 extends App {
    implicit val system = ActorSystem(Behaviors.empty, "system")
    import system.executionContext
    val log = LoggerFactory.getLogger("monLogger")

    val mergehub = MergeHub.source[String]
    /*
    val matMerge = mergehub.log("merge").toMat(Sink.ignore)(Keep.left).run()

    /* producteurs */
    Source(List("Salut !", "ça vous plait ?", "c'est finalement assez simple ?")).throttle(1, 2 seconds).log("prod1").to(matMerge).run()
    Source(List("Hello", "c'est très bizarre","mais on va s'y faire")).throttle(1, 3 seconds).log("prod2").to(matMerge).run()
    */
    /* exercice : trouver l'équivalent sous la forme de BroadcastHub ! */
    /* 5 min */

    val broadcasthub = BroadcastHub.sink[String]

    /*
    val matBcast = Source(1 to 10).throttle(1, 2 seconds).map(n => s"Hello $n").toMat(broadcasthub)(Keep.right).run()

    matBcast.log("auditeur1").runWith(Sink.ignore)
    Thread.sleep(3000)
    matBcast.log("auditeur2").runWith(Sink.ignore)
    */

    /* Combiner les deux */

    val (matMerge, matBcast) = mergehub.log("central").toMat(broadcasthub)(Keep.both).run()

    /* émetteurs */
    Source(List("A1","A2","A3")).throttle(1, 2 seconds).log("prodA").to(matMerge).run()
    Source(List("B1", "B2","B3")).throttle(1, 3 seconds).log("prodB").to(matMerge).run()
  
    /* récepteurs */
    matBcast.log("auditeur1").runWith(Sink.ignore)
    matBcast.log("auditeur2").runWith(Sink.ignore)
    matBcast.log("auditeur3").runWith(Sink.ignore)
   
    /* émetteur et récepteur : il va écrire sur le matMerge et lire depuis le matBcast */ 
    Source(List("Bonjour j'ai une histoire drôle", "à compléter")).via(Flow.fromSinkAndSource(matMerge,matBcast)).run()

    Thread.sleep(20000)
    log.info("Sayonara")
    system.terminate()    
}