package fr.mipn.fdws06

import akka.stream.scaladsl.Source
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.Sink
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors

import scala.concurrent.duration._
import scala.language.postfixOps
import akka.stream.scaladsl.RunnableGraph
import akka.stream.scaladsl.GraphDSL
import akka.NotUsed
import akka.stream.scaladsl.Broadcast
import akka.stream.ClosedShape
import org.slf4j.LoggerFactory
import akka.stream.scaladsl.Merge
import akka.stream.scaladsl.Balance
import scala.concurrent.Future
import akka.stream.FlowShape
import akka.stream.scaladsl.Keep
import scala.util.Success
import scala.util.Failure
import akka.stream.scaladsl.Partition
import akka.stream.scaladsl.Zip
import akka.stream.OverflowStrategy

/* Documentation : https://doc.akka.io/docs/akka/current/stream/stream-customize.html */


/* Exemple issu de la documentation */
import akka.stream.Attributes
import akka.stream.Outlet
import akka.stream.SourceShape
import akka.stream.stage.GraphStage
import akka.stream.stage.GraphStageLogic
import akka.stream.stage.OutHandler
import akka.stream.Inlet
import akka.stream.stage.InHandler
import scala.collection.immutable.Queue
import scala.concurrent.duration.Duration.Infinite
import akka.stream.stage.TimerGraphStageLogic

class NumbersSource(limit: Int) extends GraphStage[SourceShape[Int]] {
  val out: Outlet[Int] = Outlet("NumbersSource")
  override val shape: SourceShape[Int] = SourceShape(out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {
      // All state MUST be inside the GraphStageLogic,
      // never inside the enclosing GraphStage.
      // This state is safe to access and modify from all the
      // callbacks that are provided by GraphStageLogic and the
      // registered handlers.
      private var counter = 1

      setHandler(out, new OutHandler {
        override def onPull(): Unit = {
          push(out, counter)
          counter += 1
          if (counter > limit) complete(out) /* on ajoute une fermeture du flux */
        }
      })
    }
}
/* fin exemple issu de la documentation */

/* Exercice 1 : 
 * Essayons de généraliser l'exemple ci-dessus à un Flow 
 */
 
class MonFlow extends GraphStage[FlowShape[Int,Int]] {
  val in: Inlet[Int] = Inlet("inflow")
  val out: Outlet[Int] = Outlet("outflow")
  override val shape: FlowShape[Int,Int] = FlowShape(in,out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {
      var x = 42

      setHandler(in, new InHandler {
        override def onPush(): Unit = {
         x = grab(in)
         push(out, x)
        }
      })

      setHandler(out, new OutHandler {
        override def onPull(): Unit = {
          pull(in)
        }
      })
    }
}

/* Exercice 2 :
 * transformer le flow ci dessus en un composant Flow[Int,Double] qui répond toujours avec la moyenne des éléments 
 * qu'il a reçu. Autrement dit, s'il reçoit 1, 2, 3, il répond 2.0, si on ajoute 4, il répond 2.5. Peut-on faire 
 * en sorte que, si on lui redemande, sans avoir reçu de nouvel élément dans un certain délai il re-répond 2.5. 
 */
class AverageFlow extends GraphStage[FlowShape[Int,Double]] {
  val in: Inlet[Int] = Inlet("inflow")
  val out: Outlet[Double] = Outlet("outflow")
  override val shape: FlowShape[Int,Double] = FlowShape(in, out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {
      var sum: Double = 0
      var totalNumberOfElements = 0
      var average: Double = 0

      setHandler(in, new InHandler {
        override def onPush(): Unit = {
         val element = grab(in)
         sum += element
         totalNumberOfElements += 1
         average = sum / totalNumberOfElements
         push(out, average)
        }
      })

      setHandler(out, new OutHandler {
        override def onPull(): Unit = {
          pull(in)
        }
      })
    }
}

/* FIXME: this is not infinite, once upstream finishes, this flow finishes. */ 
class InfiniteAverageFlow(maxDelay: FiniteDuration) extends GraphStage[FlowShape[Int,Double]] {
    val in: Inlet[Int] = Inlet("inflow")
    val out: Outlet[Double] = Outlet("outflow")
    override val shape: FlowShape[Int,Double] = FlowShape(in, out)

    override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
        new TimerGraphStageLogic(shape) {
            var sum: Double = 0
            var totalNumberOfElements = 0
            var average: Double = 0 /* convention : the average of an empty list is zero */
            var tooLate = false
            val log = LoggerFactory.getLogger("com.myservice.BackendTask")

            override def onTimer(timerKey: Any): Unit = {
                tooLate = true
                log.debug("too late")
                push(out, average) /* time tu update downstream without knowing any new upstream element */
            }

            setHandler(in, new InHandler {
                override def onPush(): Unit = {
                    /* New incoming element ! We update our state… */
                    /* first cancel the timer (if running), as we want to deliver a fresh average */
                    if (isTimerActive("delay")) cancelTimer("delay")
                    val element = grab(in)
                    sum += element
                    totalNumberOfElements += 1
                    average = sum / totalNumberOfElements
                    /* update downstream except if we did it due to the delay */
                    if (!tooLate) push(out, average) /* do not push if it's too late */
                }
            })

            setHandler(out, new OutHandler {
                override def onPull(): Unit = {
                    tooLate = false
                    if (!isClosed(in)) {
                        if (!hasBeenPulled(in)) pull(in) /* do not pull if we are already pulling */
                        scheduleOnce("delay", maxDelay)
                    } else {
                        /* lets answer without delay with our old average */
                        push(out, average)
                    }
                }
            })
        }
}

object Fdws06 extends App {
    implicit val system = ActorSystem(Behaviors.empty,"system") 
    import system.executionContext
    val log = LoggerFactory.getLogger("com.myservice.BackendTask")

    /* Compléments sur les streams */
    /* 1) Un peu plus de Scala promesses et LazyList */
    // voir le worksheet fdws06.worksheet.sc
    /* 2) Créer d'autres formes de graphes que les briques prédéfinies */
    val source = Source.fromGraph(new NumbersSource(42))
    val monflow = Flow.fromGraph(new MonFlow)
    val average = Flow.fromGraph(new InfiniteAverageFlow(1 seconds))

    // Source(1 to 2).log("source").via(monflow).log("flow").throttle(1,1 seconds).runWith(Sink.ignore)
    Source(1 to 10).log("source").throttle(1, 2 seconds).via(average).log("avg").take(20).runWith(Sink.ignore)

    /* 3) Demain : utiliser les hubs */
     
    Thread.sleep(30000)

    log.info("Sayonara")
    system.terminate()
} 