package fr.mipn.fdws01

import akka.stream.scaladsl.Source
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.Sink
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors

import scala.concurrent.duration._
import scala.language.postfixOps

object Fdws01 extends App {
    implicit val system = ActorSystem(Behaviors.empty,"system") 
    import system.executionContext

    /* 1 déclaration des opérateurs */
    val source = Source(List(1,2,3))
    val flow = Flow[Int].map(_ * 2)
    val sink = Sink.foreach(println)
    // 0 -> 1 ~ 1 -> 1 = 0 - 1 
    val source2 = source.via(flow)


    /* 2 construction d'un graphe statique (ne s'exécute pas) */
    val graphe = source2.via(flow).via(flow).to(sink)

    /* 3 exécution du graphe 0 - 0 sur un acteur (un thread à part) */
    // graphe.run() // va lancer l'exécution de mon graphe sur un thread à part (celui d'un acteur)

    // List(1,2,3).map(_ * 2).foreach((x:Int) => println(s"collection $x")) // s'exécute sur le thread principal

    /* construction d'un graphe qui utilise plusieurs acteurs */
    val sourcelente = Source(1 to 10).map((x) => {Thread.sleep(500); println(s"source $x");x})
    val graphe2 = sourcelente.async.via(flow).async.via(flow).to(sink)
    //           acteur1  --> | --> acteur2  --> | --> acteur 3
    /* producteur lent, consommateurs rapides */
    graphe2.run()

    val flowlent =  Flow[Int].map((x) => {Thread.sleep(100); println(s"flow $x");x * 2})
    /* alternative : */
    val flowthrottled =  Flow[Int].map((x) => {println(s"flow $x");x * 2}).throttle(1, 0.1 second)

    val sourceplusrapide = Source(1 to 100).map((x) => {Thread.sleep(10); println(s"source $x");x})

    sourceplusrapide.async.via(flowlent).to(sink).run()
    
    Thread.sleep(10000)
    println("Sayonara")
    system.terminate()
}