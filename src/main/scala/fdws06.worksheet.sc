import scala.concurrent.{Future, Promise}
import scala.util.{Success, Failure}
import scala.concurrent.ExecutionContext.Implicits.global


/* Promesses */
val p = Promise[Int]()

p.completeWith(Future.failed((new Exception("oh no!"))))
// p

/* LazyList */
val entiers: LazyList[BigInt] = BigInt(10) #:: entiers.map(_ + 1)
//entiers(1000000)
val fibs: LazyList[BigInt] = BigInt(0) #:: BigInt(1) #:: fibs.zip(fibs.tail).map { n => n._1 + n._2 }
fibs
fibs.zip(fibs.tail)


/* Tuples */
// on peut former des tuples comme ceci
val x: (Int,Double,String) = (1, 2.0, "Hello") 
// et on peut les déconstruire comme cela 
x._2 // le deuxième élément du tuple x 
// ou comme ceci
val (a,_,c) = x



val `1+2` = 3

