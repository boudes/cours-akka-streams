
package fr.mipn.fdws.companion

/* singleton object */
object AutreExemple {
    private val secret = "je vis seul"
    def hello() = println("Hello")
}

/* companion object */
object Exemple {
    private val secret = "j'ai plein d'ami⋅es"
    def hello() = println("Hello")
    def apply(x: Int) = new Exemple(x)
    def apply() = new Exemple(0)
}

/* classe dont l'objet Exemple est compagnon */
class Exemple(x: Int) {
    def saluer(): Unit = println(s"salut $x")
    def secret() = println(Exemple.secret) // l'object compagnon n'a aucun secret pour sa classe
    // def autre_secret() = println(AutreExemple.secret) // échoue à la compilation (private)
}

object MonApplication extends App {
    AutreExemple.hello() // AutreExemple est instancié, c'est un objet singleton
    Exemple.hello() // Exemple aussi est un singleton mais il est aussi compagnon d'une classe…
    val javastyle = new Exemple(1) // une instanciation d'une classe avec new, comme en Java
    val monExemple = Exemple() // instanciation avec une méthode apply de l'objet compagnon (idiomatique en Scala !)
    monExemple.saluer()
    monExemple.secret()
}