package fr.mipn.fdws05

import akka.stream.scaladsl.Source
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.Sink
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors

import scala.concurrent.duration._
import scala.language.postfixOps
import akka.stream.scaladsl.RunnableGraph
import akka.stream.scaladsl.GraphDSL
import akka.NotUsed
import akka.stream.scaladsl.Broadcast
import akka.stream.ClosedShape
import org.slf4j.LoggerFactory
import akka.stream.scaladsl.Merge
import akka.stream.scaladsl.Balance
import scala.concurrent.Future
import akka.stream.FlowShape
import akka.stream.scaladsl.Keep
import scala.util.Success
import scala.util.Failure
import akka.stream.scaladsl.Partition
import akka.stream.scaladsl.Zip
import akka.stream.OverflowStrategy

/* Documentation : https://doc.akka.io/docs/akka/current/typed/logging.html */

object Fdws05 extends App {
    implicit val system = ActorSystem(Behaviors.empty,"system") 
    import system.executionContext
    val log = LoggerFactory.getLogger("com.myservice.BackendTask")

   /* Exercice 1 (corrigé) :
    * Créer un graphe qui distribue les éléments d'une source à l'aide d'un balance sur deux flows puis réunis
    * à l'aide d'un merge les éléments en un seul flux entrant dans un sink.
    * 
                                   /|~~> flow0 lent ~~> |\
                source0 ~~> balance |                   | Merge ~~~> sink0
                                   \|~~> flow1 ~~~~~~~> |/
   */

   val source0 = Source(1 to 10)
   val flow0 = Flow[Int].map(_ * 10).throttle(3, 1 second)
   val flow1 = Flow[Int].map(_ * 100).throttle(10, 1 second)
   val sink3 = Sink.foreach(println)

   val graphexo1 = RunnableGraph.fromGraph(
        GraphDSL.create() {
            /* 0 en paramètre un builder pour construire des formes (shapes) */
            implicit builder: GraphDSL.Builder[NotUsed] =>
            import GraphDSL.Implicits._
            /* 1 charger des formes (shapes) */
            val balance = builder.add(Balance[Int](2))
            val merge = builder.add(Merge[Int](2))
            // val flow0Shape = builder.add(flow0)
        

            /* 2 connecter par des arrêtes les formes */
            /*
                                   /|~~~~> flow0 lent ~~~> |\
                source0 ~~> balance |                      | merge ~~~> sink0
                                   \|~~~~> flow1 ~~~~~~~~> |/
            */

            source0.log("src") ~> balance ~> flow0 ~> merge; merge.out.log("merge") ~> sink3
                                  balance ~> flow1 ~> merge  
            // On utilise 
            // 3 retourner graphe clos 
            ClosedShape
        }
    )

   // graphexo1.run()

    /* Exercice 2 :
    *  Soit une liste d'entiers en source, former des paires d'éléments de la liste, réunissant un entier pair et un entier impair 
                                     /|~~~>|\
                source ~~~> partition |    | zip ~~~> sink
                                     \|~~~>|∕
    *  Selon la liste fournie en entrée que peut-il se passer ? Quel type de composant que nous n'avons pas encore vu
    *  serait intéressant ici ?
    * 
    */

    /* Corrigé de Minh Hao LE */
    val sourcea = Source(1 to 10)
    val sinka = Sink.foreach(println)
    val flow0a: Flow[Int, String, NotUsed] = Flow[Int].map(i => ("even : " + i).toString).log("flow0")
    val flow1a: Flow[Int, String, NotUsed] = Flow[Int].map(i => ("odd :" + i).toString).log("flow1")

    val graphexo2a = RunnableGraph.fromGraph(
        GraphDSL.create() {
            /* 0 en paramètre un builder pour construire des formes(shapes) */
            implicit builder: GraphDSL.Builder[NotUsed] =>
            import GraphDSL.Implicits._
            /* 1 charger des formes (shapes) */
            val partition = builder.add(Partition[Int](2, e => e % 2))
   
            val zip = builder.add(Zip[String, String])
            /* 2 connecter par des arrêtes les formes */
            /*
                                   |--- flow0 --> |
            source  ---> partition-|              | -- zip --> sink
                                   |--- flow1 --> |
            */
                 
            sourcea.log("src") ~> partition.in
            partition.out(0) ~> flow0a ~> zip.in0
            partition.out(1) ~> flow1a.buffer(100,OverflowStrategy.dropTail) ~> zip.in1

            zip.out.map { s => println("zip: "+s); s} ~> sinka
            
            // 3 retourner graphe clos 
            ClosedShape
        }
    )

    graphexo2a.run()

    /* améliorations du corrigé */
    /* Il y a un problème innérent à l'énoncé, s'il n'y pas alternance de nombres pairs et impairs dans la source
     * le flux complet sera bloqué car on ne pourra plus former de paires en sortie. On peut améliorer la situation
     * en créant des buffers qui permettront d'absorber de longues séries de nombres de même parité sans bloquer.
     */
    Thread.sleep(1000)
    log.info("Corrigé amélioré exercice 2")
    val sourceb = Source(List(1,2,4,6,4,6,8,6,8,1,3,7,5,6,6,5,4))
    val sinkb = Sink.foreach(println)
    val flow0b: Flow[Int, String, NotUsed] = Flow[Int].map(i => ("odd : " + i).toString).log("flow0")
    val flow1b: Flow[Int, String, NotUsed] = Flow[Int].map(i => ("even :" + i).toString).log("flow1")

    val graphexo2b = RunnableGraph.fromGraph(
        GraphDSL.create() {
            /* 0 en paramètre un builder pour construire des formes(shapes) */
            implicit builder: GraphDSL.Builder[NotUsed] =>
            import GraphDSL.Implicits._
            /* 1 charger des formes (shapes) */
            val partition = builder.add(Partition[Int](2, _ % 2))
   
            val zip = builder.add(Zip[String, String])
            /* 2 connecter par des arrêtes les formes */
            /*
                                   |--- flow0 --> |
            source  ---> partition-|              | -- zip --> sink
                                   |--- flow1 --> |
            */
                 
            sourcea.log("src") ~> partition.in
            partition.out(0) ~> flow0b.buffer(100,OverflowStrategy.dropTail) ~> zip.in0
            partition.out(1) ~> flow1b.buffer(100,OverflowStrategy.dropTail) ~> zip.in1

            zip.out.log("zip") ~> sinkb
            
            // 3 retourner graphe clos 
            ClosedShape
        }
    )

    graphexo2b.run()


    /***************************** Complément de cours ************************/
    /* Comment utiliser le DSL pour construire un Flow (un graphe ouvert avec une entrée et une sortie) ? */
    /* On peut construire autre chose qu'une ClosedShape à l'aide du DSL. Un sink, une source, un flow 
       … et bien d'autres formes ! 

       Pour voir comment cela fonctionne reprenons l'exemple de l'exercice 1 mais en forme ouverte, sans source
       ni sink.

                  /|~~ flow0 ~~>|\
        ~~> balance|            | merge ~~~> 
                  \|~~ flow1 ~~>|∕
     */
    
    def parallelFlows2[A,B,C,D](flow0: Flow[A,B,C], flow1: Flow[A,B,D]): Flow[A,B,(C,D)] = {
        Flow.fromGraph(
            GraphDSL.create(flow0, flow1) ((m0, m1) => (m0,m1)) {
                /* 0 en paramètre un builder pour construire des formes(shapes) */
                implicit builder => (flow0Shape, flow1Shape) =>
                import GraphDSL.Implicits._
                /* 1 charger des formes (shapes) */
                val balance = builder.add(Balance[A](2))
                val merge = builder.add(Merge[B](2))

                /* 2 connecter par des arrêtes les formes */
                balance ~> flow0Shape ~> merge
                balance ~> flow1Shape ~> merge 
    
                // 3 retourner graphe de flow (une entrée et une sortie) 
                FlowShape(balance.in,merge.out)
            }
        )
    }
    // Testons !

    val f1 = Flow[Int].scan[String]("")((s, n) => s + " " + n)
    val f2 = Flow[Int].map("d " + _)
    val f1etf2 = parallelFlows2(f1,f2)

    Source(1 to 10).via(f1etf2).log("f1 et f2").runWith(Sink.ignore)


    /* Exercice 3 :
     * a) Construire une méthode qui prenne un Flow[A,B, _] en entrée et produise un Flow[A, B, Future[Int]] en sortie
     * où la valeur matérialisée sera égale au nombre d'éléments reçus par le flow en entrée.
     * 
     * b) Améliorer votre méthode en produisant en sortie deux entiers : le nombre d'éléments reçus et le nombre
     * d'éléments émis en sortie.
     * 
     * c) Utiliser une case class FlowStats pour retourner les statistiques sur le flow
     */

    def flowWithStats[A,B, C](flow: Flow[A,B,C]): Flow[A,B,Future[Int]] = {
        val counter = Sink.fold[Int,A](0)((c,e) => c + 1)
        Flow.fromGraph(
            GraphDSL.create(counter) {
                /* 0 en paramètre un builder pour construire des formes(shapes) */
                implicit builder => counterShape =>
                import GraphDSL.Implicits._
                /* 1 charger des formes (shapes) */
                val broadcast = builder.add(Broadcast[A](2))
                val flowShape = builder.add(flow)
                /* 2 connecter par des arrêtes les formes */
                broadcast ~> flowShape
                broadcast ~> counterShape
    
                // 3 retourner graphe de flow (une entrée et une sortie) 
                FlowShape(broadcast.in,flowShape.out)
            }
        )
    }
    
    // Tester !!!
    
    /* Exercice 4 (plus difficile) : 
     * Reprendre l'exercice 1 en créeant une méthode prenant en argument une liste de Flow[A,B,_], 
     * et produisiant un Flow[A,B,List[FlowStats]] qui distribue ses éléments reçus sur les n Flow de la liste
     * et calcule des stats sur chaque Flow.
     */


    Thread.sleep(10000)
    log.info("Sayonara")
    system.terminate()
}