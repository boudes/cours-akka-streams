import akka.stream.scaladsl.Source
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.Sink
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.concurrent.Future
import scala.util.{Success, Failure}
import akka.Done
import akka.stream.scaladsl.Keep

object Fdws02 extends App {
    implicit val system = ActorSystem(Behaviors.empty, "system")
    import system.executionContext

     // Rappel sur les futurs
    val fut = Future {
        val x = 2 / 0
        12
    }

    fut.map(_ + 1).onComplete {
        case Success(n) => println(s"Bravo c'est un succès $n")
        case Failure(e) => println(s"echec $e")
    }

    // valeur matérialisée
    val sink = Sink.ignore
    val graphe = Source(List(1,2,3)).toMat(sink)((matsource,matsink) => matsink)
    
    val mat = graphe.run()
    mat.onComplete {
        case Success(x) => println(s"Bravo c'est un succès $x")
        case Failure(e) => println(s"echec $e")
    }

    List(1,2,3).foldLeft(1.0)(_ * _)
    // utilisation pour récupérer une valeur calculée par l'exécution du flux de données
    val source = Source(List(1,2,3))
    val sink2 = Sink.fold[Double, Int](0.0)(_ + _)

    val mat2 = source.toMat(sink2)(Keep.right).run()

    mat2.onComplete  {
        case Success(x) => println(s"Bravo le fold est un succès $x")
        case Failure(e) => println(s"le fold est un échec $e")
    }

    // Exercice 1 calculer factorielle 10 en utilisant une source émettant des entiers de 1 à 10

    /* Exercice 2 :
    - créer une source composée d'une liste de textes
    - utiliser une matérialisation pour compter le nombre de mots de l'ensemble des textes 
    */
    // hint : val mots = "êtes-vous là ?".split(" ")

    // Voici un peu de sucre syntaxique bien utile
    //  val mat2 = source.toMat(sink2)(Keep.right).run()
    val mat3 = source.runWith(sink2)
    val flow = Flow[Int].map(_ * 2)
    val mat4 = flow.runWith(source,sink)
    val mat5 = source.toMat(sink2)(Keep.right).run()


    Thread.sleep(10000)
    println("Sayonara")
    system.terminate()

}