package fr.mipn.fdws12

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import org.slf4j.LoggerFactory
import akka.stream.scaladsl.MergeHub
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Keep
import scala.concurrent.duration._
import scala.language.postfixOps
import akka.stream.scaladsl.Source
import akka.stream.scaladsl.BroadcastHub
import akka.stream.scaladsl.Flow
import java.nio.file.Paths
import scala.concurrent.Future
import akka.stream.scaladsl.FileIO
import akka.stream.IOResult
import akka.stream.alpakka.csv.scaladsl.CsvParsing
import akka.stream.KillSwitches


object Fdws12 extends App {
    implicit val system = ActorSystem(Behaviors.empty, "system")
    import system.executionContext
    val log = LoggerFactory.getLogger("monLogger")

    val source = Source.tick(0 seconds, 3 seconds, "hello")
    val switch = KillSwitches.single[String]
    val sink = Sink.ignore 

    
    /* kill switch */
    /*
    val sw = source.log("tick").viaMat(switch)(Keep.right).to(sink).run()
    Thread.sleep(10000)
    sw.shutdown()
    */
    def createDynamicFlow[A](name: String) = {
        val mergehub = MergeHub.source[A]
        val broadcasthub = BroadcastHub.sink[A]
        val switchableFlow = KillSwitches.single[A] 
        val ((talk, kill), listen) = mergehub.log(name)
          .viaMat(switchableFlow)(Keep.both)
          .toMat(broadcasthub)(Keep.both)
          .run()
      (talk, listen, kill)  
    }

    /* substreams et groupBy */
    Source(1 to 10).groupBy(Int.MaxValue, _ % 3)
    .mergeSubstreams
    .fold("")((s,e)=> s + s"-$e")
    .to(Sink.foreach(println)) //.run()


    /* FileIO avec alpakka */
    val file = Paths.get("example.csv") /* entier id, entier année, chaîne formation */
    FileIO.fromPath(file) /* 1,2020,M1 */
    .via(CsvParsing.lineScanner()) /* List("1","2020","M1")*/
    .map(_.map(_.utf8String)).log("alpakka")
   // .runWith(Sink.ignore)

    /* définir le format des données */
    case class Inscription(id: Int, annee: Int, formation: String)
    FileIO.fromPath(file) 
    .via(CsvParsing.lineScanner()) /* List("1","2020","M1")*/
    .map(_.map(_.utf8String).toVector)
    .map((xs) => Inscription(xs(0).toInt, xs(1).toInt, xs(2)))
    .log("data")
    .runWith(Sink.ignore)

    /* La sortie qu'on attend sur le fichier fourni est 
    */

    Thread.sleep(30000)
    log.info("Sayonara")
    system.terminate()    
}