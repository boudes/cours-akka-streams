package fr.mipn.fdws03

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.stream.scaladsl.GraphDSL
import akka.stream.ClosedShape
import akka.stream.scaladsl.Broadcast
import akka.NotUsed
import akka.stream.scaladsl.Source
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.RunnableGraph
import scala.concurrent.Future

object Fdws03 extends App {

    implicit val system = ActorSystem(Behaviors.empty, "system")
    
    /* Comment faire d'autres formes de graphes (que des chaînes 0-1…1-0) ? */

    val source1 = Source(1 to 10)
    val source2 = Source(15 to 20)
    val sink1 = Sink.foreach((x: Int) => println(s"sink1: $x"))
    val sink2 = Sink.foreach((x: Int) => println(s"sink2: $x"))
    val sink3 = Sink.foreach((x: Int) => {Thread.sleep(1000);println(s"sink3: $x")})

    
    val graph = RunnableGraph.fromGraph(
        GraphDSL.create() {
            /* 0 en paramètre un builder pour construire des formes(shapes) */
            implicit builder: GraphDSL.Builder[NotUsed] =>
            import GraphDSL.Implicits._
            /* 1 charger des formes (shapes) */
            // Première forme une forme 1-2, c'est à dire une entrée, deux sorties
            val broadcast = builder.add(Broadcast[Int](2))
            
            /* 2 connecter par des arrêtes les formes */
            /*
                                   |--- sink1
            source1 ---> broadcast-|
                                   |--- sink2
            */
            source1 ~> broadcast ~> sink1
                       broadcast ~> sink2
            
            source2 ~> sink3           

            // 3 retourner graphe clos 
            ClosedShape
        }
    )
    graph.run()

    // Exercice : Faire un graphe avec Merge et Balance.

    Thread.sleep(10000)
    println("Sayonara")
    system.terminate()
}


/* 
Catégorie : objets (d'une certaine sorte) + des flèches entre objets
avec une notion de composition de flèches et des identités
L'exemple canonique c'est : les ensembles et les fonctions entre ensembles
avec la composition des fonctions.
L'exemple informatique c'est la catégorie P des "programmes" :
objet = type (classe) et flèche = fonction informatique.

Quand on a deux catégories : C et D.
On peut avoir un foncteur F entre C et D qui est une fonction qui associe
à chaque objet X de C un objet FX de D et à chaque flèche de C f: X1 -> X2
une flèche de D Ff: FX1 à FX2.

On a en particulier des endofoncteurs qui vont de C dans C (la catégorie d'arrivée est
égale à la catégorie de départ).

Option est un endofoncteur de la catégorie P.
Parce que si A est une type, Option[A] est un type. Le type dont les éléments
sont des Some(a) ou a est une élément de A ou bien None.

Si f : A => B alors Option(f): Option[A] => Option[B] est simplement _.map(f).

Option est un endofoncteur, List est une sorte d'Endofoncteur variadique
Future est un foncteur, etc. tous tous tous tous sans exception ont comme forme sur
les flèches la méthode que l'on a défini sous le nom map.

f: A => B

map sur les options :
ao: Option[A]
ao.map(f) == ao matche {
    case Option(a) => Option(f(a))
    case None => None
}

map sur les futures :
af: Future[A]
af.map(f) == af.onComplete {
    case Success(a) => Future.Success(f(a))
    case Failure(e) => Future.Failure(e)
}

Monade : c'est un endofoncteur M avec en plus deux méthodes (qui vérifient certaines propriétés) :
flatMap (aussi appelée bind), et une return (en Scala, la apply de l'objet companion).
return : A => MA
bind : (A => MB) => (MA => MB),
flatMap : MA => (A => MB) => MB, ma.flatMap(f: A => MB): MB
*/
