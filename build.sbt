name := "cours-akka-stream"

version := "1.0"

scalaVersion := "2.13.4"

lazy val AkkaVersion = "2.6.10"
lazy val AkkaHttpVersion = "10.2.1"
lazy val ScalaTestVersion = "3.2.2"
lazy val AlpakkaVersion = "2.0.2"

libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
    "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
    "com.typesafe.akka" %% "akka-stream-typed" % AkkaVersion,
    "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
    "com.lightbend.akka" %% "akka-stream-alpakka-csv" % AlpakkaVersion,
//    "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    "org.scalatest" %% "scalatest" % ScalaTestVersion,
    //    "com.typesafe.akka" %% "akka-testkit" % AkkaVersion, // remplacé par akka-actor-testkit-typed
    "com.typesafe.akka" %% "akka-actor-testkit-typed" % AkkaVersion,
    "com.typesafe.akka" %% "akka-stream-testkit" % AkkaVersion
)

// scalacOptions := Seq("-unchecked", "-deprecation")